﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Timers;
using System.Configuration;
using System.IO;

namespace SiteCall
{
    public class PagePeriоdCaller
    {
        private Timer myTimer;
        private string queryUrl;
        private PageParser pageGetter;
        private CaseProvider provider;

        public PagePeriоdCaller() 
        {
            SettingsReader settingsGetter = new SettingsReader();
            pageGetter = new PageParser();
            provider = new CaseProvider();
            queryUrl = settingsGetter.GetUrl();

            myTimer = new Timer();
            myTimer.Elapsed += myTimer_Elapsed;
            myTimer.Interval = settingsGetter.GetTimeout();
            myTimer.Enabled = true;
        }

        public void StartTimer()
        {
            myTimer.Start();
        }

        private void myTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<Case> currentCases = pageGetter.GetCasesAfterParsing().ToList();
            if (currentCases.Count > 0) 
            {
                provider.InsertOrUpdateDbList(currentCases);
            }      
        }
    }
}
