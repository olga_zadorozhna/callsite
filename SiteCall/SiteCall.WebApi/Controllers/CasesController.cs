﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Http.Cors;




namespace SiteCall.WebApi.Controllers
{
    [EnableCors(origins: "http://localhost:60422", headers: "*", methods: "*")]
    public class CasesController : ApiController
    {
        private CaseProvider provider = new CaseProvider();
        // GET api/values
      
        public IHttpActionResult Get(int offset = 0, int limit = 10, int quantity = 20, string order = "Id", bool reverse = true, string search="")
        {
            var result = provider.GetLastCases(offset, limit, quantity, order, reverse, search);
            return Ok(result);
        }     

        // POST api/values
        public void Post([FromBody]Case newCase)
        {
            provider.InsertSingleCase(newCase);
        }

        // PUT api/cases/5  
        public void Put([FromBody]Case newCase)
        {
            provider.UpdateSingleCase(newCase);
        }
        
        // DELETE api/values/5
        public void Delete(int id)
        {
            provider.DeleteCase(id);
        }

        //public void Delete(string CaseId)
        //{
        //    provider.DeleteCaseByProjectCaseId(CaseId);
        //}

        //public IEnumerable<Case> Get(int quantity)
        //{
        //    return provider.GetLastCases(quantity);
        //}

        // GET api/values/5

        //public Case Get(int id)
        //{
        //    return provider.GetCase(id);
        //}

        //public Case Get(string CaseId)
        //{
        //    return provider.GetCaseByProjectCaseId(CaseId);
        //}
    }
}