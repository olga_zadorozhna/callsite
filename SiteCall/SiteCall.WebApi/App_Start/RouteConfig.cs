﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;

namespace SiteCall.WebApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{quantity}/{offset}/{limit}",

                defaults: new { controller = "Home", action = "Index", 
                                id = UrlParameter.Optional, 
                                quantity = UrlParameter.Optional,
                                offset = UrlParameter.Optional,
                                limit = UrlParameter.Optional,
                                order = UrlParameter.Optional,
                                reverse = UrlParameter.Optional,
                                search = UrlParameter.Optional
                }
            );
        }       
    }
}