﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Http;
using System.Net;

namespace SiteCall
{
    public class CaseProvider
    {
        public void InsertOrUpdateDbList(IEnumerable<Case> caseList)
        {
            var ids = caseList.Select(c => c.ProjectCaseId);
            //HashSet<long> idsHash = new HashSet<long>(ids);
            HashSet<string> idsHash = new HashSet<string>(ids);
            HashSet<Case> caseListHash = new HashSet<Case>(caseList);

            using (var db = new CaseContext())
            {
                var dbCases = db.Cases.Where(c => idsHash.Contains(c.ProjectCaseId)).ToList();
                foreach (var dbCase in dbCases)
                {
                    if (idsHash.Contains(dbCase.ProjectCaseId) && !caseListHash.Contains(dbCase))
                    {
                        var newCase = caseList.Single(c => c.ProjectCaseId == dbCase.ProjectCaseId);
                        newCase.Id = dbCase.Id;
                        db.Entry(dbCase).CurrentValues.SetValues(newCase);
                    }
                }
                var dbIds = dbCases.Select(w => w.ProjectCaseId);
                //HashSet<long> dbIdsHash = new HashSet<long>(dbIds);
                HashSet<string> dbIdsHash = new HashSet<string>(dbIds);
                var missedCases = caseList.Where(c => !dbIdsHash.Contains(c.ProjectCaseId));
                db.Cases.AddRange(missedCases);
                db.SaveChanges();

                foreach (var cs in missedCases)
                {
                    Console.WriteLine(cs.ToString());
                }
                Console.WriteLine("Pushed {0} cases", missedCases.ToList().Count());    
            }
        }

        public void InsertOrUpdateSingleCase(Case newCase) 
        {
            List<Case> caseList = new List<Case>();
            caseList.Add(newCase);
            InsertOrUpdateDbList(caseList);
        }

        public void UpdateSingleCase(Case newCase) 
        {
            using (var db = new CaseContext())
            {
                var dbCases = db.Cases.SingleOrDefault(c => c.ProjectCaseId == newCase.ProjectCaseId);
                if (dbCases != null)
                {
                    newCase.Id = dbCases.Id;
                    db.Entry(dbCases).CurrentValues.SetValues(newCase);
                    db.SaveChanges();
                }
            }
        }

        public void InsertSingleCase(Case newCase)
        {
            using (var db = new CaseContext())
            {
                var dbCases = db.Cases.SingleOrDefault(c => c.ProjectCaseId == newCase.ProjectCaseId);
                if (dbCases == null)
                {
                    db.Cases.Add(newCase);
                    db.SaveChanges();
                }
            }
        }
            
        public void DeleteCase(int id) 
        {
            using (var db = new CaseContext())
            {
                var myCase = db.Cases.SingleOrDefault(c => c.Id == id);

                if (myCase != null)
                {
                    db.Cases.Remove(myCase);
                }
                db.SaveChanges();
            }
        }

        public void DeleteCaseByProjectCaseId(string Caseid)
        {
            using (var db = new CaseContext())
            {
                var myCase = db.Cases.SingleOrDefault(c => c.ProjectCaseId == Caseid);

                if (myCase != null)
                {
                    db.Cases.Remove(myCase);
                }
                db.SaveChanges();
            }
        }

        public Case GetCase(int id) 
        {
            using (var db = new CaseContext())
            {
                return db.Cases.SingleOrDefault(s => s.Id == id);
            }
        }

        public Case GetCaseByProjectCaseId(string CaseId)
        {
            using (var db = new CaseContext())
            {
                return db.Cases.SingleOrDefault(s => s.ProjectCaseId == CaseId);
            }
        }

        public IEnumerable<Case> GetAllCases() 
        {
            using (var db = new CaseContext())
            {
                var Cases = db.Cases.Select(c => c).ToList();
                return Cases;
            }
        }


        public IEnumerable<Case> GetLastCases(int quantity)
        {
            using (var db = new CaseContext())
            {
                var Cases = db.Cases.Select(c => c).ToList();
                var lengthToRemove = Cases.Count() - quantity;
                if (lengthToRemove > 0) 
                {
                    Cases.RemoveRange(0, lengthToRemove);
                }             
                return Cases;
            }
        }

        public Object GetLastCases(int offset, int limit, int quantity, string order, bool reverse, string search)
        {
            using (var db = new CaseContext())
            {
                var Cases = db.Cases.Select(c => c).ToList();
                var lengthToRemove = Cases.Count() - quantity;
                if (lengthToRemove > 0)
                {
                    Cases.RemoveRange(0, lengthToRemove);
                }
                var total = Cases.Count();
                if (search != null && search != "")
                {
                    Cases = Cases.Where(c => c.Customer.ToUpper().Contains(search.ToUpper())).ToList();
                }
                 var data = GetOrderedList(Cases, order, reverse).Skip(offset).Take(limit);
               
                return (new
                {
                    Data = data,
                    Paging = new
                    {
                        Total = total,
                        Limit = limit,
                        Offset = offset,
                        Returned = data.Count()
                    }
                });
            }
        }


        public enum Field 
        {
            Id,
            ProjectCaseId,
            CaseCode,
            Url,
            Customer,
            Headline,
            GBU,
            IBU,
            Positions,
            Proposed,
            Booked,
            Status,
            Created,
            Updated
        }

        public List<Case> GetOrderedList(IEnumerable<Case> caseList, string order, bool reverse)
        {
            List<Case> result = new List<Case>();
            switch (order)
            {
                case "Id":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Id).ToList() : caseList.OrderByDescending(c => c.Id).ToList();
                        break;
                    }
                case "ProjectCaseId":
                    {
                        result = reverse ? caseList.OrderBy(c => c.ProjectCaseId).ToList() : caseList.OrderByDescending(c => c.ProjectCaseId).ToList();
                        break;
                    }
                case "CaseCode":
                    {
                        result = reverse ? caseList.OrderBy(c => c.CaseCode).ToList() : caseList.OrderByDescending(c => c.CaseCode).ToList();
                        break;
                    }
                case "Url":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Url).ToList() : caseList.OrderByDescending(c => c.Url).ToList();
                        break;
                    }
                case "Customer":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Customer).ToList() : caseList.OrderByDescending(c => c.Customer).ToList();
                        break;
                    }
                case "Headline":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Headline).ToList() : caseList.OrderByDescending(c => c.Headline).ToList();
                        break;
                    }
                case "GBU":
                    {
                        result = reverse ? caseList.OrderBy(c => c.GBU).ToList() : caseList.OrderByDescending(c => c.GBU).ToList();
                        break;
                    }
                case "IBU":
                    {
                        result = reverse ? caseList.OrderBy(c => c.IBU).ToList() : caseList.OrderByDescending(c => c.IBU).ToList();
                        break;
                    }
                case "Positions":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Positions).ToList() : caseList.OrderByDescending(c => c.Positions).ToList();
                        break;
                    }
                case "Proposed":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Proposed).ToList() : caseList.OrderByDescending(c => c.Proposed).ToList();
                        break;
                    }
                case "Booked":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Booked).ToList() : caseList.OrderByDescending(c => c.Booked).ToList();
                        break;
                    }
                case "Status":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Status).ToList() : caseList.OrderByDescending(c => c.Status).ToList();
                        break;
                    }
                case "Created":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Created).ToList() : caseList.OrderByDescending(c => c.Created).ToList();
                        break;
                    }
                case "Updated":
                    {
                        result = reverse ? caseList.OrderBy(c => c.Updated).ToList() : caseList.OrderByDescending(c => c.Updated).ToList();
                        break;
                    }
                default: result = caseList.OrderBy(c => c.Id).ToList();
                    break;
            }
            return result;
        }
    }
}
