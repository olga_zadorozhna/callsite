﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Timers;
using System.Text.RegularExpressions;
using System.Web;
using HtmlAgilityPack;
using System.Net;
using System.IO;


namespace SiteCall
{
    public class PageParser
    {
        private string html;
        private HtmlDocument document = new HtmlDocument();

        public IEnumerable<Case> GetCasesAfterParsing() 
        {
            IEnumerable<Case> caseList = new List<Case>();
            GetHtmlFromFile(@"D:\Projects\Projects\CallSite\Cases\Cases.html");
            this.document.LoadHtml(this.html);
            var rows = GetRows(this.document);
            if (rows != null)
            {
                caseList = rows.Select(c => ParseRow(c));
            }
            return caseList;
        }

        #region Helpers
    
        private void GetHtmlFromFile(string path)
        {
            using (StreamReader reader = new StreamReader(File.Open(path, FileMode.Open)))
            {
                this.html = reader.ReadToEnd().ToString();
            }
        }

        private void GetHtmlFromUrl(string url)
        {
            this.html = "";
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    this.html = responseContent.ReadAsStringAsync().Result;
                }
            }
        }

        private HtmlNodeCollection GetRows(HtmlDocument doc)
        {
            var rows = doc.DocumentNode.SelectNodes("//table//tr[@id='sectionRow_0_']");
            return rows;
        }

        private Case ParseRow(HtmlNode row)
        {
            string[] str = new string[12];
            
            var CaseTd = row.SelectNodes("td");
            var CaseCodeHref = CaseTd[0].SelectSingleNode("a[@href]");
            str[0] = (CaseTd[0] != null) ? CaseTd[0].InnerText.Trim() : "";
            str[1] = (CaseCodeHref != null) ? CaseCodeHref.Attributes["href"].Value : "#";
            for (int i = 2; i < 11; i++) 
            {
                str[i] = (CaseTd[i - 1] != null) ? CaseTd[i - 1].InnerText.Trim() : "";
            }
            str[11] = row.Attributes["data-case-id"].Value;          
            Case singleCase = new Case(str);
            return singleCase;
        }
 
        #endregion
    }
}
