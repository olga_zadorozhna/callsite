﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SiteCall
{
    public class SettingsReader
    {
        public string GetUrl() 
        {
            return ConfigurationManager.AppSettings["Url"];
        }

        public int GetTimeout()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);
        }

        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
        }

    }
}
