﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SiteCall;

namespace Test.SiteCall
{
    public class PageCasesGetterTest
    {
        PageParser pageGetter = new PageParser();

        [Fact]
        public void GetCasesTest()
        {   
            Case fourthCase = pageGetter.GetCasesAfterParsing().ToList()[4];

            string[] testArray = new string[12];
            testArray[0] = "ALTV-C4C";
            testArray[1] = "https://upsa.epam.com/workload/projectCaseEdit.do?projectCaseId=4060741400034515507";
            testArray[2] = "Altevie Technologies S.r.l.";
            testArray[3] = "SAP CRM Cloud";
            testArray[4] = "GBU/EU/B";
            testArray[5] = "No IBU";
            testArray[6] = "1";
            testArray[7] = "1 /\n             0";
            testArray[8] = "Open";
            testArray[9] = "15-Jun-2015";
            testArray[10] = "06-Jul-2015";
            testArray[11] = "4060741400034515507";
            
            Case testCase = new Case(testArray);
            Assert.Equal(fourthCase.ToString(), testCase.ToString());  
        }    
    }
}
