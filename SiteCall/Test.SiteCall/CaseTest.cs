﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SiteCall;

namespace Test.SiteCall
{
    public class CaseTest
    {
        [Fact]
        public void ParseCandidateTest() 
        {
            string[] testArray = new string[12];
            for (int i = 0; i < 8; i++ )
            {
                testArray[i] = "1";
            }
            testArray[9] = "10 Jun 2015";
            testArray[10] = "10 Jun 2015";
            testArray[7] = "15/       \n            3";
            testArray[11] = "4060741400034515507";

            Case testCase = new Case(testArray);

            Assert.Equal(testCase.Proposed, 15);
            Assert.Equal(testCase.Booked, 3);            
        }
    }
}
