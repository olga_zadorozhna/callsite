﻿(function () {
    angular
        .module('casesApp')
        .directive('createCase', createCase);

    function createCase() {
        return {
            restrict: 'AE',
            scope: {
            },
            controller: CreateController,
            controllerAs: 'create',
            templateUrl: "../Scripts/directives/createCase.html"
        };
    };

    CreateController.$inject = ['dataService'];

    function CreateController(dataService) {
        var create = this;

        create.createClicked = false;
        create.createCase = createCase;
        create.clearForm = clearForm;
        create.addCase = addCase;
        create.cancelAdd = cancelAdd;
        create.caseFields = [
            "Id", "ProjectCaseId", "CaseCode", "Url", "Customer", "Headline", "GBU",
            "IBU", "Positions", "Proposed", "Booked", "Status", "Created", "Updated"
        ];

        function clearForm() {
            angular.extend(create, dataService.emptyCase());
        };

        function addCase() {
            var myCase = angular.extend({}, create);
            dataService.addCase(myCase).success(function () {
                alert("Case was succesfully added");
                create.clearForm();
                create.createClicked = false;
            });
        };

        function cancelAdd() {
            create.clearForm();
            create.createClicked = false;
        };

        $(function () {
            $('.datepicker').datepicker();
        });

        function createCase() {
            create.createClicked = !create.createClicked;
        };
    };
            
}());
