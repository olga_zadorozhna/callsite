﻿(function () {
    angular
        .module('casesApp')
        .directive('showActivePage', showActivePage);

    function showActivePage() {
        return function (scope, element, attrs) {
            $(document).ready(function () {
                $('.pagination li:first').addClass('active');

            });
            element.on('click', function () {
                console.log(this);
                $('.pagination li').removeClass('active');
                $(this).parent('li').addClass('active');
            });
        };
    };
})();
