﻿(function () {

    angular
        .module('casesApp')
        .directive('displayCase', displayCase);

    function displayCase() {
        return {
            restrict: 'A',
            scope: {
                info: '=',
                index: '=',             
            },
            controller: DisplayController,
            controllerAs: 'vm',
            templateUrl: "../Scripts/directives/displayCase.html"        
            };
    };

    DisplayController.$inject = ['dataService', '$scope'];

    function DisplayController(dataService, $scope) {
        var vm = this;
        
        vm.changeMode = changeMode;        
        vm.mode = true;
        vm.deleteCase = deleteCase;
        vm.updateCase = updateCase;
        vm.editCase = editCase;
        vm.cancelEdit = cancelEdit;
        vm.copyCase = {};

        function changeMode() {
            vm.mode = !vm.mode;
        };

        function updateCase(myCase) {
            dataService.updateCase(myCase).success(function () {
                changeMode();
                alert("Case was successfully updated");
            });
        };

        function deleteCase(myCase, index) {
            dataService.deleteCase(myCase.Id).success(function () {
                $scope.$parent.removeFromList(myCase, index);
            });
        };

        function editCase(myCase) {
            changeMode();
            vm.copyCase = angular.copy(myCase);
            $(".datepickerEdit").datepicker({
                format: "ll"
            });
        };

        function cancelEdit(myCase, index) {
            changeMode();
            $scope.$parent.replaceCase(vm.copyCase, index);
        };
    };
}());

