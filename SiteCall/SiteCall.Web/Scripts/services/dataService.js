﻿(function () {
    angular
    .module('casesApp')
    .service('dataService', dataService);

    dataService.$inject = ['$http'];

    function dataService($http) {

        var service = {
            getCases: getCases,
            getLastCases: getLastCases,
            deleteCase: deleteCase,
            updateCase: updateCase,
            addCase: addCase,
            emptyCase: emptyCase,
        };

        function getCases() {
            return $http.jsonp('http://localhost:60345/api/cases?quantity=20&callback=JSON_CALLBACK')
            .then(getCasessComplete);
        };

        function getLastCases(offset, predicate, reverse, quantity, searchString) {
            return $http.jsonp('http://localhost:60345/api/cases?offset=' + offset + '&order=' + predicate + '&reverse=' + !reverse +
                        '&quantity=' + quantity + '&search=' + searchString + '&callback=JSON_CALLBACK')
            .then(getCasessComplete);
        };

        function getCasessComplete(response) {
            return response.data
        };

        function deleteCase(Id) {
            return $http({
                method: 'DELETE',
                url: 'http://localhost:60345/api/cases/' + Id
            }).
            success(function (data) {
                    console.log("deleted");
            })
            .error(function (error) {
                console.log(error.message);
            });
        };

        function updateCase(myCase) {
            return $http({
                method: 'PUT',
                url: 'http://localhost:60345/api/cases/' + myCase.Id,
                data: myCase
            }).
                success(function (data) {
                    console.log("updated");
                })
                .error(function (error) {
                    console.log(error.message);
                });
        };

        function addCase(myCase) {
           return $http({
                method: 'POST',
                url: 'http://localhost:60345/api/cases/' + myCase.Id,
                data: myCase
            }).
            success(function (data) {
                console.log("added");
            })
            .error(function (error) {
                console.log(error.message);
            });
        };

        function emptyCase() {
            return {
                ProjectCaseId: null,
                CaseCode: null,
                Url: null,
                Customer: null,
                Headline: null,
                GBU: null,
                IBU: null,
                Positions: null,
                Proposed: null,
                Booked: null,
                Status: null,
                Created: null,
                Updated: null
            }
        };

       return service;
    };
}());




