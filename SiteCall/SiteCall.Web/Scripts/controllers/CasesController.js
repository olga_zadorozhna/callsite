﻿(function () {
    angular
        .module('casesApp')
        .controller("CasesController", CasesController);

    CasesController.$inject = ['dataService', '$scope'];

    function CasesController(dataService, $scope) {
        var me = this;

        me.topCases = [];
        me.paging = [];
        me.offset = 0;

        activate();

        me.searchString = "";
        me.getNextPage = getNextPage;
        me.getCases = getCases;
        me.sorting = [];
        me.enableSorting = enableSorting;
        me.order = order;     
        me.caseFields = [
            "Id", "ProjectCaseId", "CaseCode", "Url", "Customer", "Headline", "GBU",
            "IBU", "Positions", "Proposed", "Booked", "Status", "Created", "Updated"
        ];
        var reverse = true;

        function activate() {
            return getInititalCases();
        };

        function getNextPage(offset) {
            GetLastCases(offset);
        };

        function getInititalCases() {
            return dataService.getCases()
                .then(function (data) {
                    getInitialValues(data);
                });
        };

        function getCases() {
            GetLastCases();
            disableSorting();
            resetStartPage();
        };

        function enableSorting(i) {
            for (var j = 0; j < me.caseFields.length; j++) {
                me.sorting[j] = false;
            };
            me.sorting[i] = !me.sorting[i];
        };

        function disableSorting() {
            for (var j = 0; j < me.caseFields.length; j++) {
                me.sorting[j] = false;
            };
        };

        function getRange(total, perPage) {
            var range = [];
            var end = Math.ceil(total / perPage);
            for (var i = 0; i < end; i++) {
                range.push(i);
            };
            return range;
        };

        function order(predicate) {
            reverse = (me.predicate === predicate) ? !reverse : false;
            me.predicate = predicate;
            GetLastCases();
            resetStartPage();
        };

        function GetLastCases(offset) {
            return dataService.getLastCases(offset, me.predicate, reverse, me.quantity, me.searchString)
                .then(function (data) {
                    getInitialValues(data);
                });
        };

        function resetStartPage() {
            $('.pagination li').removeClass('active');
            $('.pagination li:first').addClass('active');
        };

        function getInitialValues(data) {
            me.topCases = data.Data,
            me.paging = data.Paging,
            me.range = getRange(data.Paging.Total, data.Paging.Limit);
            me.quantity = data.Paging.Total;
            me.offset = data.Paging.Offset;
        };

        $scope.replaceCase = function (myCase, index) {
            me.topCases.splice(index, 1);
            me.topCases.splice(index, 0, myCase);
        };

        $scope.removeFromList = function (myCase, index) {
            me.topCases.splice(index, 1);
        };
    };
}());
