namespace SiteCall
{
    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Text;

   
    public partial class Case : IEquatable<Case>
    {
        public int Id { get; set; }

        //public long ProjectCaseId { get; set; }

        [Required]
        [StringLength(255)]
        public string ProjectCaseId { get; set; }


        [Required]
        [StringLength(255)]
        public string CaseCode { get; set; }

        [Required]
        [StringLength(255)]
        public string Url { get; set; }

        [Required]
        [StringLength(255)]
        public string Customer { get; set; }

        [Required]
        [StringLength(255)]
        public string Headline { get; set; }

        [Required]
        [StringLength(255)]
        public string GBU { get; set; }

        [Required]
        [StringLength(255)]
        public string IBU { get; set; }

        public int Positions { get; set; }

        public int Proposed { get; set; }

        public int Booked { get; set; }

        [Required]
        [StringLength(255)]
        public string Status { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public Case()
        {
            
        }

        public Case(string[] arr)
        {
            ProjectCaseId = arr[11]; // long.Parse(arr[11]);
            CaseCode = arr[0];
            Url = arr[1];
            Customer = arr[2];
            Headline = arr[3];
            GBU = arr[4];
            IBU = arr[5];
            Positions = Int32.Parse(arr[6]);
            int proposed, booked;
            ParseCandidates(arr[7], out proposed, out booked);
            Proposed = proposed;
            Booked = booked;
            Status = arr[8];
            Created = DateTime.Parse(arr[9]).Date;
            Updated = DateTime.Parse(arr[10]).Date;
        }

        private void ParseCandidates(string str, out int proposed, out int booked)
        {
            int indexOfFirstChar = str.IndexOf("/");
            proposed = Int32.Parse(str.Substring(0, indexOfFirstChar));
            int indexOfLastSign = str.LastIndexOf(" ");
            booked = Int32.Parse(str.Substring(indexOfLastSign, str.Length - indexOfLastSign));
        }

       

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("ProjectCaseId = {0}{1}", ProjectCaseId, Environment.NewLine)
                .AppendFormat("CaseCode = {0}{1}", CaseCode, Environment.NewLine)
                .AppendFormat("Url = {0}{1}", Url, Environment.NewLine)
                .AppendFormat("Customer = {0}{1}", Customer, Environment.NewLine)
                .AppendFormat("Headline = {0}{1}", Headline, Environment.NewLine)
                .AppendFormat("GBU = {0}{1}", GBU, Environment.NewLine)
                .AppendFormat("IBU = {0}{1}", IBU, Environment.NewLine)
                .AppendFormat("Positions = {0}{1}", Positions, Environment.NewLine)
                .AppendFormat("Proposed = {0}{1}", Proposed, Environment.NewLine)
                .AppendFormat("Booked = {0}{1}", Booked, Environment.NewLine)
                .AppendFormat("Status = {0}{1}", Status, Environment.NewLine)
                .AppendFormat("Created = {0}{1}", Created.ToString("d-MMM-yyyy"), Environment.NewLine)
                .AppendFormat("Updated = {0}{1}", Updated.ToString("d-MMM-yyyy"), Environment.NewLine);

            return sb.ToString();
        }

        public bool Equals(Case other)
        {
            return (this.ToString() == other.ToString());
        }
    }
}
