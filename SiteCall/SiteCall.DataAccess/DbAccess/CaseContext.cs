namespace SiteCall
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    

    public partial class CaseContext : DbContext
    {
        public CaseContext()
            : base("name=CaseContext")
        {
        }

        public virtual DbSet<Case> Cases { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
